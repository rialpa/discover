# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-25 00:48+0000\n"
"PO-Revision-Date: 2022-08-31 22:50+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: kcm/package/contents/ui/main.qml:32
#, kde-format
msgid "Update software:"
msgstr "Actualisa software:"

#: kcm/package/contents/ui/main.qml:33
#, kde-format
msgid "Manually"
msgstr "Manualmente"

#: kcm/package/contents/ui/main.qml:43
#, kde-format
msgid "Automatically"
msgstr "Automaticamente"

#: kcm/package/contents/ui/main.qml:50
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Software updates will be downloaded automatically when they become "
"available. Updates for applications will be installed immediately, while "
"system updates will be installed the next time the computer is restarted."
msgstr ""
"Actualisationes de  software essera discargate automaticamente quando illos "
"deveni disponibile. Actualisationes per applicationes essera installate "
"immediatemente, ma le actualisationes de systema essera installate le "
"proxime vice que le computator es reinitialisate."

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Update frequency:"
msgstr "Frequentia de actualisation:"

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Notification frequency:"
msgstr "Frequentia de notification:"

#: kcm/package/contents/ui/main.qml:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "Daily"
msgstr "Cata die"

#: kcm/package/contents/ui/main.qml:65
#, kde-format
msgctxt "@item:inlistbox"
msgid "Weekly"
msgstr "Cata septimana"

#: kcm/package/contents/ui/main.qml:66
#, kde-format
msgctxt "@item:inlistbox"
msgid "Monthly"
msgstr "Cata mense"

#: kcm/package/contents/ui/main.qml:67
#, kde-format
msgctxt "@item:inlistbox"
msgid "Never"
msgstr "Jammais"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid "Use offline updates:"
msgstr "Usa actualisationes foras de linea:"

#: kcm/package/contents/ui/main.qml:123
#, kde-format
msgid ""
"Offline updates maximize system stability by applying changes while "
"restarting the system. Using this update mode is strongly recommended."
msgstr ""
"Actualisationes foras de linea maximisa le stabilitate de systema per "
"applicar cambios durante que il reinitia le systema. Usa iste modo de "
"actuaisar es fortemente recommendate."

#: kcm/updates.cpp:31
#, kde-format
msgid "Software Update"
msgstr "Actualisationes de Software"

#: kcm/updates.cpp:33
#, kde-format
msgid "Configure software update settings"
msgstr "Configura preferentias de actualisar software"

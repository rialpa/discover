# Language  translations for discover package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the discover package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-25 00:48+0000\n"
"PO-Revision-Date: 2022-02-21 21:04+0100\n"
"Last-Translator: mkkDr2010 <mkondarev@yahoo.de>\n"
"Language-Team: MK\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: notifier/DiscoverNotifier.cpp:144
#, kde-format
msgid "View Updates"
msgstr "Преглед на актуализациите"

#: notifier/DiscoverNotifier.cpp:256
#, kde-format
msgid "Security updates available"
msgstr "Налични актуализации на защитата"

#: notifier/DiscoverNotifier.cpp:258
#, kde-format
msgid "Updates available"
msgstr "Налични актуализации"

#: notifier/DiscoverNotifier.cpp:260
#, kde-format
msgid "System up to date"
msgstr "Системата е актуална"

#: notifier/DiscoverNotifier.cpp:262
#, kde-format
msgid "Computer needs to restart"
msgstr "Компютърът трябва да се рестартира"

#: notifier/DiscoverNotifier.cpp:264
#, kde-format
msgid "Offline"
msgstr "Офлайн"

#: notifier/DiscoverNotifier.cpp:266
#, kde-format
msgid "Applying unattended updates…"
msgstr "Прилагане на неконтролирани актуализации…"

#: notifier/DiscoverNotifier.cpp:300
#, kde-format
msgid "Restart is required"
msgstr "Необходимо е рестартиране"

#: notifier/DiscoverNotifier.cpp:301
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr ""
"Системата трябва да бъде рестартирана, за да влязат в сила актуализациите."

#: notifier/DiscoverNotifier.cpp:307
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Рестартиране"

#: notifier/DiscoverNotifier.cpp:327
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Надграждане"

#: notifier/DiscoverNotifier.cpp:328
#, kde-format
msgid "Upgrade available"
msgstr "Налице е надграждане"

#: notifier/DiscoverNotifier.cpp:329
#, kde-format
msgid "New version: %1"
msgstr "Нова версия: %1"

#: notifier/main.cpp:39
#, kde-format
msgid "Discover Notifier"
msgstr "Discover Notifier"

#: notifier/main.cpp:41
#, kde-format
msgid "System update status notifier"
msgstr "Уведомител за състоянието на системната актуализация"

#: notifier/main.cpp:43
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Plasma Development Team"

#: notifier/main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "M.k"

#: notifier/main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "radnev@yahoo.com"

#: notifier/main.cpp:52
#, kde-format
msgid "Replace an existing instance"
msgstr "Заменяне на съществуваща инстанция"

#: notifier/main.cpp:54
#, kde-format
msgid "Do not show the notifier"
msgstr "Без показване на известия"

#: notifier/main.cpp:54
#, kde-format
msgid "hidden"
msgstr "скрити"

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr "Актуализации"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover…"
msgstr "Отваряне на Discover…"

#: notifier/NotifierItem.cpp:55
#, kde-format
msgid "See Updates…"
msgstr "Преглед на актуализации…"

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Refresh…"
msgstr "Обновяване…"

#: notifier/NotifierItem.cpp:64
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Рестартирайте, за да приложите инсталирани актуализации"

#: notifier/NotifierItem.cpp:65
#, kde-format
msgid "Click to restart the device"
msgstr "Кликнете, за да рестартирате устройството"
